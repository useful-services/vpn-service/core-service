package config

import (
	"fmt"
	"log/slog"
	"os"
	"strings"

	"github.com/spf13/viper"
)

const (
	configName = "application"
	configType = "yaml"
	configPath = "config"

	ymlDelimiter = "."
	envDelimiter = "_"
)

func getEnvReplacer() *strings.Replacer {
	return strings.NewReplacer(ymlDelimiter, envDelimiter)
}

func configure() {
	viper.SetConfigName(configName)
	viper.SetConfigType(configType)
	viper.AddConfigPath(configPath)
	viper.SetEnvKeyReplacer(getEnvReplacer())
	viper.AutomaticEnv()
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("fatal error. config file: %w", err))
	}
}

type App struct {
	Log *log `mapstructure:"log"`
}

type log struct {
	Level string `mapstructure:"level"`
}

func NewApp() (*App, error) {
	configure()
	app := &App{}
	err := viper.Unmarshal(app)
	if err != nil {
		return nil, fmt.Errorf("[app.NewApp] Couldn't parse config: %w", err)
	}

	lvl := slog.LevelDebug
	err = lvl.UnmarshalText([]byte(app.Log.Level))
	if err != nil {
		slog.Error(err.Error())
		lvl = slog.LevelWarn
	}
	slog.SetDefault(slog.New(slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{Level: lvl})))

	return app, nil
}
