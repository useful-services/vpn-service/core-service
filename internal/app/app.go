package app

import (
	"core-vpn-service/internal/config"
	"fmt"
)

type App struct {
	*config.App
}

func NewApp() *App {
	cfg, err := config.NewApp()
	if err != nil {
		panic(fmt.Errorf("Couldn't start app: %w", err))
	}

	return &App{
		cfg,
	}
}